.. highlight:: console

.. _qmp-shell-wrap:

qmp-shell-wrap
==============

.. autodocstring:: qemu.qmp.qmp_shell.main_wrap
   :noindex:
